'use strict';

pokemonApp.component('pokemonDetail', {

    controller: function PokemonDetailCtrl($scope, $routeParams, PokemonsService) {

        $scope.pokemonLoaded = false;

        $scope.pokemon = PokemonsService.get({
            pokemonId: $routeParams['pokemonId']
        }, function(successResult) {
            // Окей!
            $scope.notfoundError = false;
            $scope.pokemonLoaded = true;

            $scope.activeTab = 1;
            $scope.disableControlTab = true;
        }, function(errorResult) {
            // Не окей..
            $scope.notfoundError = true;
            $scope.pokemonLoaded = true;


        });

        $scope.pokemon.$promise.then(function(result) {
            //$scope.pokemonLoaded = true;
        });

        $scope.deletePokemon = function(pokemonId) {

            $scope.pokemon.$delete({
                pokemonId: pokemonId
            }, function(successResult) {
                // Окей!
                $scope.deletionSuccess = true;
            }, function(errorResult) {
                // Не окей..
                $scope.deletionError = true;
            });

        }


    },

    template: `<div ng-if="!pokemonLoaded">
    <span class="glyphicon glyphicon-refresh"></span>
    Loading...
</div>

<div class="alert alert-danger" role="alert" ng-if="notfoundError">Покемон не найден</div>

<uib-tabset active="activeTab">

    <uib-tab heading="Инфо" disable="disableControlTab">

        <h1 class="media-heading">{{pokemon.name | uppercase }}</h1>
        <p>Вес:
            {{pokemon.weight}}, рост:
            {{pokemon.height}}</p>

    </uib-tab>

    <uib-tab heading="Операции" >

        <button class="btn btn-danger" ng-click="deletePokemon(pokemon.objectId)">
            <span class="glyphicon glyphicon-trash"></span>
            Удалить
        </button>
        <a class="btn btn-primary" href="#!/edit/{{pokemon.objectId}}">
            Редактировать
        </a>

    </uib-tab>

</uib-tabset>

<div class="alert alert-success" role="alert" ng-if="deletionSuccess">Пока, покемон!</div>
<div class="alert alert-danger" role="alert" ng-if="deletionError">Покемоны не сдаются!</div>`

});
